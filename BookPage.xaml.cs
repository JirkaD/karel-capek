﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace KarelCapek
{
    public partial class BookPage : PhoneApplicationPage
    {
        private string _bookText;
        private int _index;
        private int _currentPage;
        private ApplicationBarIconButton PreviousButton2;
        private ApplicationBarIconButton NextButton2;
        private string bookname = @"Books/bila_nemoc.txt";

        public BookPage()
        {
            InitializeComponent();
            InitAppBar();
        }

        private void InitAppBar()
        {
            ApplicationBar appBar = new ApplicationBar();

            PreviousButton2 = new ApplicationBarIconButton(new Uri("icons/appbar.back.rest.png", UriKind.Relative));
            PreviousButton2.Click += new EventHandler(PreviousButton_Click);
            PreviousButton2.Text = "Předchozí";
            appBar.Buttons.Add(PreviousButton2);

            NextButton2 = new ApplicationBarIconButton(new Uri("icons/appbar.next.rest.png", UriKind.Relative));
            NextButton2.Click += new EventHandler(NextButton_Click);
            NextButton2.Text = "Následující";
            appBar.Buttons.Add(NextButton2);

            ApplicationBar = appBar;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            bookname = NavigationContext.QueryString["filename"];
            if (_bookText == null)
            {
                var resource = Application.GetResourceStream(new Uri(bookname, UriKind.Relative));
                var streamReader = new StreamReader(resource.Stream);
                _bookText = streamReader.ReadToEnd();
                // _bookText = _bookText.Substring(0, 1000);
                streamReader.Close(); base.OnNavigatedTo(e);
            }

            if (State.ContainsKey("Index"))
            {
                _index = (int)State["Index"];
            }

            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(bookname))
                _currentPage = (int)settings[bookname];
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            State["Index"] = _index;
            IsolatedStorageSettings.ApplicationSettings[bookname] = _currentPage;
            base.OnNavigatedFrom(e);
        }

        private void PhoneApplicationPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            PrepareContent();
        }

        private void PrepareContent()
        {
            PreviousButton2.IsEnabled = _currentPage != 0;

            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(bookname + _currentPage))
                _index = (int)settings[bookname + _currentPage];
            else
                settings[bookname + _currentPage] = _index;
            FillPageWithText(ref _index);

            NextButton2.IsEnabled = _index != _bookText.Length;
        }

        private void FillPageWithText(ref int index)
        {
            Stack.Children.Clear();
            const double lineHeight = 26.6033325195312;
            double verticalSpace = 696 - 6;
            while (verticalSpace > lineHeight)
            {
                var line = new TextBlock();
                Stack.Children.Add(line);
                GetLine(line, _bookText, ref index, 456);
                verticalSpace -= lineHeight;
            }
        }

        private void GetLine(TextBlock line, string bookText, ref int index, int lineWidth)
        {
            while (true)
            {
                if (bookText.Length == index)
                    return;
                if (bookText[index] == '\n')
                {
                    index++;
                    if (string.IsNullOrEmpty(line.Text))
                        line.Text = " ";
                    return;
                }
                if (bookText[index] == '\r')
                {
                    index++;
                    continue;
                }
                var previousText = line.Text;
                var previousIndex = index;

                do
                {
                    if (!string.IsNullOrEmpty(line.Text) || bookText[index] != ' ')
                        line.Text += bookText[index];
                    index++;
                } while (bookText.Length > index && bookText[index] != ' ' && bookText[index] != '\r' && bookText[index] != '\n');

                if (line.ActualWidth > lineWidth)
                {
                    if (!string.IsNullOrEmpty(previousText))
                    {
                        line.Text = previousText;
                        index = previousIndex;
                    }
                    return;
                }
            }
        }

        private void NextButton_Click(object sender, System.EventArgs e)
        {
            if (_index != _bookText.Length)
            {
                _currentPage++;
                PrepareContent();
            }
        }

        private void PreviousButton_Click(object sender, System.EventArgs e)
        {
            if (_currentPage > 0)
            {
                _currentPage--;
                PrepareContent();
            }
        }
    }
}