﻿using System;
using System.Windows.Controls;
using Microsoft.Phone.Controls;

namespace KarelCapek
{
    public class Book
    {
        public string BookName { get; set; }

        public string FileName { get; set; }
    }

    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var lb = (ListBox)sender;
            int i = lb.SelectedIndex;
            if (i != -1)
            {
                var book = (Book)lb.SelectedItem;
                try
                {
                    NavigationService.Navigate(new Uri(String.Format("/BookPage.xaml?filename={0}", book.FileName), UriKind.Relative));
                }
                catch
                {
                }
            }
            lb.SelectedIndex = -1;
        }
    }
}